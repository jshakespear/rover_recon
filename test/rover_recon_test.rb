require 'minitest/autorun'
require_relative '../lib/rover_recon'

class TestRoverRecon < Minitest::Test
  def setup
    @rr = RoverRecon.new
  end

  def test_calculate_direction
    current = 'N'
    instruction = 'L'
    new_direction = @rr.calculate_direction(current, instruction)
    assert_equal('W', new_direction)
  end

  def test_execute_instructions
    bounds = [5, 5]
    rover = { position: { coordinates: [ 1, 2 ], direction: 'N' }, instruction: ['L', 'M', 'L', 'M', 'L', 'M', 'L', 'M', 'M'] }
    new_rover = @rr.execute_instructions(bounds, rover)
    assert_equal({ coordinates: [ 1, 3 ], direction: 'N' }, new_rover[:position])
  end

  def test_invalid_execute_instructions
    bounds = [5, 5]
    rover = { position: { coordinates: [ 1, 2 ], direction: 'N' }, instruction: ['L', 'M', 'M', 'M', 'L', 'M', 'M'] }
    exception = assert_raises(RuntimeError) {
      @rr.execute_instructions(bounds, rover)
    }
    assert_equal('Error: rover instruction exceeds plateau bounds', exception.message)
  end

  def test_explore_plateau
    bounds = [5, 5]
    rovers = [ { position: { coordinates: [ 1, 2 ], direction: 'N' }, instruction: ['L', 'M', 'L', 'M', 'L', 'M', 'L', 'M', 'M'] } ]
    new_rovers = @rr.explore_plateau(bounds, rovers)
    assert_equal({ coordinates: [ 1, 3 ], direction: 'N' }, new_rovers.first[:position])
  end

  def test_parse_bounds
    bounds = @rr.parse_bounds("5 5\n")
    assert_equal([5, 5], bounds)
  end

  def test_parse_incorrect_bounds
    exception = assert_raises(RuntimeError) {
      @rr.parse_bounds("5 5 5\n")
    }
    assert_equal('Error: invalid bounds specified', exception.message)
  end

  def test_parse_non_integer_bounds
    exception = assert_raises(RuntimeError) {
      @rr.parse_bounds("A 5\n")
    }
    assert_equal('Error: invalid bounds specified', exception.message)
  end

  def test_parse_instruction
    instruction = @rr.parse_instruction("LMLMLMLMM\n")
    assert_equal(['L', 'M', 'L', 'M', 'L', 'M', 'L', 'M', 'M'], instruction)
  end

  def test_parse_invalid_instruction
    exception = assert_raises(RuntimeError) {
      @rr.parse_instruction("LMFLMLMLMM\n")
    }
    assert_equal('Error: invalid instruction specified', exception.message)
  end

  def test_parse_position
    position = @rr.parse_position("1 2 N\n")
    assert_equal({ coordinates: [1, 2], direction: 'N' }, position)
  end

  def test_parse_invalid_position
    exception = assert_raises(RuntimeError) {
      @rr.parse_position("N 1 2\n")
    }
    assert_equal('Error: invalid position specified', exception.message)
  end

  def test_valid_position
    bounds = [5, 5]
    coordinates = [3, 2]
    valid = @rr.valid_position?(bounds, coordinates)
    assert_equal(true, valid)
  end

  def test_incorrect_valid_position
    bounds = [5, 5]
    coordinates = [6, 5]
    valid = @rr.valid_position?(bounds, coordinates)
    assert_equal(false, valid)
  end
end
