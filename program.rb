#!/usr/bin/env ruby

require './lib/rover_recon'

rr = RoverRecon.new
bounds = rr.parse_bounds(ARGF.first.strip)
rovers = rr.process_input(ARGF)

rr.explore_plateau(bounds, rovers).each do |rover|
  position = rover[:position]
  puts "#{position[:coordinates][0]} #{position[:coordinates][1]} #{position[:direction]}"
end

