class RoverRecon

  # Calculate new direction based on current after new instruction
  def calculate_direction(current, instruction)
    rose = ['N', 'E', 'S', 'W']
    location = rose.index(current)
    case instruction
    when 'L'
      location -= 1
    else 'R'
      location += 1
      if location > 3
        location -= rose.length
      end
    end
    rose[location]
  end

  # Execute instruction set moving or turning as instructed
  def execute_instructions(bounds, rover)
    position = rover[:position]
    rover[:instruction].each do |inst|
      if inst == 'M'
        case position[:direction]
        when 'N'
          position[:coordinates] = [ position[:coordinates][0], position[:coordinates][1] + 1 ]
        when 'E'
          position[:coordinates] = [ position[:coordinates][0] + 1, position[:coordinates][1] ]
        when 'S'
          position[:coordinates] = [ position[:coordinates][0], position[:coordinates][1] - 1 ]
        else # 'W'
          position[:coordinates] = [ position[:coordinates][0] - 1, position[:coordinates][1] ]
        end
      else
        position[:direction] = calculate_direction(position[:direction], inst)
      end
      if !valid_position?(bounds, position[:coordinates])
        raise 'Error: rover instruction exceeds plateau bounds'
      end
    end

    rover[:position] = position
    rover
  end

  # Loop through rovers and execute instruction sets then output each rover's final position
  def explore_plateau(bounds, rovers)
    rovers.each do |rover|
      rover = execute_instructions(bounds, rover)
    end
    rovers
  end

  # Get initial plateau bounds
  def parse_bounds(line)
    if line.match(/^\d\s\d$/)
      line.split(' ').map(&:to_i)
    else
      raise 'Error: invalid bounds specified'
    end
  end

  # Verify instructions match expected regex and parse
  def parse_instruction(line)
    if line.match(/^[LMR]+$/)
      line.scan(/./)
    else
      raise 'Error: invalid instruction specified'
    end
  end

  # Verify specified initial position and parse
  def parse_position(line)
    if pos = line.match(/^(\d\s\d)\s([ENSW])$/)
      { coordinates: pos[1].split(' ').map(&:to_i), direction: pos[2] }
    else
      raise 'Error: invalid position specified'
    end
  end

  # Parse input from specified directives
  def process_input(args)
    instruction = nil
    position = nil
    rovers = []

    args.each_line do |line|
      # Break on new line to ensure all rovers are specified
      if line == "\n"
        break
      end

      # If second ARGF line (first and subsequent rover position) parse and validate
      if args.lineno % 2 == 0
        begin
          position = parse_position(line.strip)
        rescue RuntimeError => e
          puts e.message
          break
        end 
      else
        # If third ARGF line (second and subsequent rover instruction set) parse and validate
        begin
        instruction = parse_instruction(line.strip) 
        rescue RuntimeError => e
          puts e.message
          break
        end
        rovers << { position: position, instruction: instruction } 
        position = nil
        instruction = nil
      end
    end
    rovers
  end

  # Determine if current coordinates are valid based on specified plateau bounds
  def valid_position?(bounds, coordinates)
    if coordinates[0] >= 0 && coordinates[1] >= 0
      if coordinates[0] <= bounds[0] && coordinates[1] <= bounds[1]
        return true
      end
    end
    false
  end
end
